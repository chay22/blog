<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ExampleTest extends TestCase
{
    /**
     * A basic functional test example.
     *
     * @return void
     */
    public function testBasicExample()
    {
        $this->visit('/register')
             ->type('Taylor', 'username')
             ->type('taylor@taylor.com', 'email')
             ->type('Abcd1234', 'password')
             ->type('Abcd1234', 'password_confirmation')
             ->press('Register')
             ->seePageIs('/');
    }
}
