var elixir = require('laravel-elixir');
var gutils = require('gulp-util');
var browserify = elixir.config.js.browserify;
var es2015 = require('babel-preset-es2015');
/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

browserify.transformers.push(
  { name: 'babelify', options: { presets: [es2015] } },
  { name: 'vueify', options: {} }
);


if(elixir.config.production == true){
    process.env.NODE_ENV = 'production';
}

if(gutils.env._.indexOf('watch') > -1 && elixir.config.production != true){
    elixir.config.js.browserify.plugins.push({
        name: "browserify-hmr",
        options : {}
    });
}

elixir(function(mix) {

    mix.less('app.less', 'resources/assets/css');
    mix.sass('theme.scss', 'resources/assets/css');
    mix.sass('auth.scss', 'resources/assets/css');
    
    mix.styles([
        'app.css',
        'prism.css',
        'theme.css',
      ], 'public/css/app.css');

    mix.styles([
        'app.css',
        'auth.css',
      ], 'public/css/auth.css');

    mix.browserify('app.js', 'public/js/main.js');

    mix.scripts([
      'prism.js',
      '../../../public/js/main.js',
    ], 'public/js/app.js');

    if(gutils.env._.indexOf('watch') > -1 && elixir.config.production != true){
        mix.browserSync({
           files: [
               elixir.config.appPath + '/**/*.php',
               elixir.config.get('public.css.outputFolder') + '/**/*.css',
               elixir.config.get('public.versioning.buildFolder') + '/rev-manifest.json',
               'resources/views/**/*.php'
           ],
           proxy: 'werron.dev',
        });
    }

    mix.version([
          'css/app.css',
          'css/auth.css',
    			'js/app.js',
    ]);
});