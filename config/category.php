<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Main Category
    |--------------------------------------------------------------------------
    |
    | Also can be called as parent category which is, of course has no child.
    | Set the name as correct as you should since it's a display name.
    |
    */

    ['name' => 'Pakaian'],


    /*
    |--------------------------------------------------------------------------
    | Sub Category
    |--------------------------------------------------------------------------
    |
    | Lies beneath its main. You must provide parent as key and the name
    | of parent correctly. Parent name must be defined first on Main Category.
    |
    */

    ['name' => 'Kaos', 'parent' => 'Pakaian'],
    ['name' => 'Swim Wear', 'parent' => 'Pakaian'],
];
