<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default role
    |--------------------------------------------------------------------------
    |
    | Set the default user role per registration to be. Make sure to it has 
    | been defined first on role list below.
    | 
    | example: 'user'
    | 
    */

    'default' => 'user',

    /*
    |--------------------------------------------------------------------------
    | Super privillege role
    |--------------------------------------------------------------------------
    |
    | Set the super privillege role. This role won't be restricted on any page.
    | Make sure to define this role first on role list
    | 
    | example: 'admin'
    | 
    */
    'super' => 'admin',

    /*
    |--------------------------------------------------------------------------
    | Role list
    |--------------------------------------------------------------------------
    |
    | List of roles available to begin page restriction.
    | 
    | example: ['admin', 'user']
    | 
    */

    'roles' => [
                'admin',
                'user',
               ],
];
