<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Series extends Model
{
    /**
     * Apply relationship
     *
     * @return collection
     */
    public function posts()
    {
        return $this->hasMany('App\Post');
    }
}
