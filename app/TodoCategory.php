<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TodoCategory extends Model
{
    protected $fillable = ['name'];

    protected $visible = [
        'id', 'name',
    ];

    /**
     * Define relationship to App\Todo
     * 
     * @return void
     */
    public function todos()
    {
    	return $this->hasMany('App\Todo');
    }
}
