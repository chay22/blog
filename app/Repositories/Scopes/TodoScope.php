<?php

namespace App\Repositories\Scopes;


trait TodoScope
{
	/**
	 * Fetch query including relational table and sorting
	 * 
	 * @param  object  $query
	 * @param  bool    $relationship
	 * @return \Illuminate\Database\Eloquent\Builder
	 */
	public function scopeGetAll($query, $relationship = true)
	{
		if ($relationship) {
			return $query->with('TodoCategory')->get();
		}

		return $query->get();
	}

}
