<?php

namespace App\Repositories\Scopes;


trait IpListScope
{
	/**
	 * Fetch blacklisted IP address list
	 * 
	 * @param  object  $query
	 * @return \Illuminate\Database\Eloquent\Builder
	 */
	public function scopeBlacklist($query)
	{
		return $query->where('blacklist', 1)->get();
	}
	/**
	 * Fetch whitelisted IP address list
	 * 
	 * @param  object  $query
	 * @return \Illuminate\Database\Eloquent\Builder
	 */
	public function scopeIsBlacklisted($query, $ip)
	{
		$address = $query->where('address', $ip);
		$count = $address->count();

		return ($count == 0) ?: $address->get()->address;
	}

}
