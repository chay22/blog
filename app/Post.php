<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
    use SoftDeletes;

    /*
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'slug', 'image', 'excerpt', 'content',
        'user_id', 'series_id', 'published_at',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at', 'updated_at', 'deleted_at',
        'published_at',
    ];

    /**
     * Apply relationship
     *
     * @return collection
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Apply relationship
     *
     * @return collection
     */
    public function tags()
    {
        return $this->belongsToMany('App\Tag');
    }

    /**
     * Apply relationship
     *
     * @return collection
     */
    public function series()
    {
        return $this->belongsTo('App\Series');
    }
}
