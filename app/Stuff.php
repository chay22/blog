<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Repositories\DescCreatedAtGlobalScope as CreatedAtDesc;

class Stuff extends Model
{

	/**
	 * Define allowed table for mass assignment
	 * 
	 * @var array $fillable
	 */
    protected $fillable = [
    	'name', 'url', 'description', 'category'
    ];


	/**
	 * The "booting" method of the model.
	 *
	 * @return void
	 */
	protected static function boot()
	{
		parent::boot();

        static::addGlobalScope(new CreatedAtDesc);
	}
}
