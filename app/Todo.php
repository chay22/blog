<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Repositories\Scopes\TodoGlobalScope;
use App\Repositories\DescCreatedAtGlobalScope as CreatedAtDesc;
use App\Repositories\Scopes\TodoScope;
use Carbon\Carbon;


class Todo extends Model
{
	use TodoScope;

	/**
	 * Define allowed table for mass assignment
	 * 
	 * @var array $fillable
	 */
    protected $fillable = [
    	'task', 'status',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at', 'updated_at', 'deleted_at'
    ];

    /**
     * Attributes that should be hidden
     *
     * @var array
     */
    protected $hidden = [
    	'todo_category_id', 'deleted_at'
    ];

    /**
     * Append new attributes
     * 
     * @var array
     */
    protected $appends = [
        'category', 'strike'
    ];

	/**
	 * The "booting" method of the model.
	 *
	 * @return void
	 */
	protected static function boot()
	{
		parent::boot();

        static::addGlobalScope(new CreatedAtDesc);
		static::addGlobalScope(new TodoGLobalScope);
	}

    /**
     * Define the inverse of relationship
     * 
     * @return void
     */
    public function todoCategory()
    {
    	return $this->belongsTo('App\TodoCategory');
    }

    /**
     * Transform date `created_at`
     *
     * @param string $value
     * @return string
     */
    public function getCreatedAtAttribute($value)
    {
        $time = Carbon::parse($value)->diffForHumans();
        return $this->attributes['created_at'] = $time;
    }

    /**
     * Transform date `updated_at`
     *
     * @param string $value
     * @return string
     */
    public function getUpdatedAtAttribute($value)
    {
        $time = Carbon::parse($value)->diffForHumans();
        return $this->attributes['updated_at']
             = $this->attributes['created_at']
           !== $time ? $value : null;
    }

    /**
     * Transform status attribute
     *
     * @param string $value
     * @return string
     */
    public function getStatusAttribute($value)
    {
        return $this->attributes['status'] = strtoupper($value);
    }

    /**
     * Push todo category to its todo task
     *
     * @param string $value
     * @return string
     */
    public function getCategoryAttribute($value)
    {   
        $name = $this->todoCategory()->get()[0]->name;
        return $this->attributes['category'] = $name;
    }

    /**
     * Create new attribute strike
     *
     * @param string $value
     * @return bool
     */
    public function getStrikeAttribute($value)
    {   
        return $this->attributes['strike']
             = $this->attributes['status'] === 'done';
    }

}
