<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Repositories\Scopes\IpListScope;

class IpList extends Model
{
	use IpListScope;
	
	/**
	 * Define current table name
	 * 
	 * @var  string  $table
	 */
	protected $table = 'ip_blacklists';

	/**
	 * Set current table primary index other than id
	 * 
	 * @var  bool.false  $incrementing
	 */
	public $incrementing = false;

	/**
	 * The attributes that should be casted to native types.
	 *
	 * @var array
	 */
	protected $casts = [
	    'blacklist' => 'boolean',
	];

}
