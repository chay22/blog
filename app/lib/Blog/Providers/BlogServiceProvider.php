<?php
namespace Blog\Providers;
 
use Illuminate\Support\ServiceProvider;
 
class BlogServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(
            'Blog\Repositories\Post\Contract\Post',
            'Blog\Repositories\Post\Eloquent'
        );

        $this->app->bind(
            'Blog\Services\TextToHTML\Parser',
            'Blog\Services\TextToHTML\CommonMark'
        );
    }
}
