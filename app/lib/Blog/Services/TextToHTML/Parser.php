<?php

namespace Blog\Services\ParseText;

interface Parser
{
    /**
     * Render a string of text
     *
     * @param string $text
     * @return string
     */
    public function render($string);
}
