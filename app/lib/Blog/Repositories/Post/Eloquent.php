<?php

namespace Blog\Repositories\Post;

use Blog\Repositories\Post\Contract\Post as PostContract;
use App\Post;

class Eloquent implements PostContract
{
    /**
     * Get all posts
     * @return \App\Post
     */
    public function all()
    {
        return Post::all();
    }
 
     /**
     * Find single post
     * @param  int $id
     * @return \App\Post
     */
    public function find($id)
    {
        return Post::find($id);
    }
  
     /**
     * Find post with 'where' clause
     * @param  mixed  $column
     * @param  string $operator
     * @param  string $value
     * @param  bool   $boolean
     * @return \App\Post
     */
    public function where($column, $operator = null, $value = null, $boolean = 'and')
    {
        return Post::find($id);
    }
 
     /**
     * Insert new post
     * @param  array $request
     * @return \App\Post
     */
    public function create($request)
    {
        return Post::create($request);
    }
     
     /**
     * Insert new post
     * @param  int   $id
     * @param  array $request
     * @return \App\Post
     */
    public function update($id, array $request)
    {
        $post =  Post::find($id);

        foreach ($request as $column => $value) {
            $post->{$column} = $value;
        }

        return $post->save();
    }
}
