<?php

namespace Blog\Repositories\Post\Contract;

interface Post
{
    /**
     * Get all posts
     * @return \Blog\Repositories\Post\Contract class
     */
    public function all();
 
     /**
     * Find single post
     * @param  int $id
     * @return \Blog\Repositories\Post\Contract class
     */
    public function find($id);
 
 
     /**
     * Find post with 'where' clause
     * @param  mixed  $column
     * @param  string $operator
     * @param  string $value
     * @param  bool   $boolean
     * @return \Blog\Repositories\Post\Contract class
     */
    public function where($column, $operator, $value, $boolean);

     /**
     * Insert new post
     * @param  array $request
     * @return \Blog\Repositories\Post\Contract class
     */
    public function create($request);
     
     /**
     * Insert new post
     * @param  int   $id
     * @param  array $request
     * @return \Blog\Repositories\Post\Contract class
     */
    public function update($id, array $request);
}
