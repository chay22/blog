<?php
/**
 * Laravel 5 validation extended
 *
 * @author    Cahyadi Nugraha <cnu@protonmail.com>
 * @link      https://enchay.ru  author site
 * @link      https://github.com/chay22/Validy  github site
 * @license   MIT
 */
namespace Valididy;

use Illuminate\Contracts\Validation\Factory as LaravelValidator;
use Illuminate\Contracts\Config\Repository;
use Illuminate\Support\Str;
use ReflectionClass;
use Exception;
use ReflectionMethod;
use HTMLPurifier_ConfigSchema;
use HTMLPurifier_Config;
use HTMLPurifier;

class Factory
{
    protected $repository;

    protected $validator;

    protected $rules = [
        Valididy::class,
    ];

    protected $replacers = [
        ValididyReplacer::class,
    ];

    public function __construct(LaravelValidator $validator, Repository $repository)
    {
        $this->validator = $validator;
        $this->repository = $repository;

    }

    public function addRule()
    {
        return $this->parseClass($this->rules);
    }

    public function addReplacer()
    {
        return $this->parseClass($this->replacers);
    }

    protected function parseClass($classes)
    {
        foreach ($classes as $class) {
            $reflector = new ReflectionClass($class);

            if (! $reflector->isInstantiable()) {
                throw new Exception("Cannot instantiate $class class");
            }

            $this->getMethods($reflector->getMethods(), $class);
        }
    }

    protected function getMethods(array $methods, $class)
    {
        foreach ($methods as $method) {
            $name = $method->name;

            if (Str::startsWith($name, 'register')) {

                $this->registerRules($class, $name);

            } elseif (Str::startsWith($name, 'replace')) {

                $this->registerReplacer($class, $name);
            }
        }
    }

    protected function registerRules($class, $name)
    {
        $rule = Str::snake(str_replace('register', '', $name));

        $message = ($class === Valididy::class) ? Message::get($rule) : null;

        return $this->validator->extendImplicit($rule, "$class@$name", $message);
    }

    protected function registerReplacer($class, $name)
    {
        $rule = Str::snake(str_replace('replace', '', $name));

        return $this->validator->replacer($rule, "$class@$name");
    }
}
