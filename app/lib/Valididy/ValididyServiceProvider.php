<?php

namespace Valididy;

use Illuminate\Support\ServiceProvider;

class ValididyServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $validator = new Factory($this->app['validator'], $this->app['config']);
        $validator->addRule();
        $validator->addReplacer();
/*
        $this->app['validator']->replacer('similar', function($message, $attribute, $rule, $parameters) {
            return str_replace(':other', $parameters[0], $message);
        });*/
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
