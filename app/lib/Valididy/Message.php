<?php

namespace Valididy;

final class Message
{
    protected static $message = [
        'purify'        => 'The :attribute field contains invalid value.',
        'similar'       => 'Looks like the :attribute field is too similar with :other value.',
        'active_email'  => 'The :attribute format is neither valid nor active email address.',
    ];

    public static function get($rule)
    {
        return self::$message[$rule];
    }
}
