<?php

namespace Valididy;

use Illuminate\Support\Arr;

class Valididy extends Validator
{
    public function registerActiveEmail($attribute, $value, $parameters, $validator)
    {
        if (! filter_var($value, FILTER_VALIDATE_EMAIL)) {
            return false;
        }

        $host = substr(strrchr($value, '@'), 1);

        return count(dns_get_record($url, DNS_MX)) > 0;
    }

    public function registerSimilar($attribute, $value, $parameters, $validator)
    {
        $this->requireParameterCount(1, $parameters, 'similar');

        $other = Arr::get($validator->getData(), $parameters[0]);

        if (strlen($other) < 255 && strlen($value) < 255) {
            $lev = levenshtein(metaphone(strtolower($other)), metaphone(strtolower($value)));
            $lev = 1 - $lev / max(strlen($other), strlen($value));
            
            return intval($lev * 100) <= 90;
        }

        similar_text($other, $value, $percent);

        return intval($percent) <= 85;
    }

    public function registerPurify($attribute, $value, $parameters, $validator)
    {
        return true;
    }
}
