<?php

namespace Valididy;

class ValididyReplacer extends Replacer
{
    public function replaceSimilar($message, $attribute, $rule, $parameters)
    {
        return str_replace(':other', $parameters[0], $message);
    }
}
