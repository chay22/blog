<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ExtendsValidationRules extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //$this->app['validator']->extend('purify', 'Valididy\Validator@validatePurify');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
