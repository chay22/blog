<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class BlogCreation extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'bail|required|max:100',
            'slug' => 'present|alpha_dash|max:120',
            'tag' => 'required|array',
            'image' => 'required|image|mimetypes:image/jpeg,image/png',
            'excerpt' => 'required|max:800',
            'content' => 'required'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'excerpt.max' => 'Post excerpt is too long.',
            'image.mimetypes' => 'Supported image file are jpeg, jpg or png.'
        ];
    }
}
