<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class AuthorizedRoles
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  array  $roles
     * @return mixed
     */
    public function handle($request, Closure $next, ...$roles)
    {
        $user = $request->user();

        if ($user && $this->isAllowed($user, $roles)) {
            return $next($request);
        }
        
        return view('errors.403');
    }

    /**
     * Check if current user role is allowed 
     * 
     * @param  string      $user    current requested user
     * @param  null|array  $roles
     * @return bool
     */
    protected function isAllowed($user, $roles)
    {
        $roles = $this->extractRoles($roles);

        //Check for role exsistance
        if (! $this->roleExists($roles)) {
            return false;
        }

        //Override this check if user has super privillege
        if ($user->role === $this->superRole()) {
            return true;
        }

        foreach ($roles as $role) {
            if (str_contains($role, $user->role)) {
                return true;
            }  
        }

        return false;
    }

    /**
     * Convert given roles to an array
     * 
     * @param  null|array  $roles
     * @return array
     */
    protected function extractRoles($roles)
    {
        $super = $this->superRole();
        foreach($roles as &$role)
            $roles = strtolower($role);

        //if no argument set on middleware return
        //the super role
        return count($roles) == 0 ? [$super] : $roles;
    }

    /**
     * Check if role is defined in config file
     * 
     * @param  null|array  $roles
     * @return bool
     */
    protected function roleExists($roles)
    {
        $roleList = config('roles.roles');
        foreach($roleList as &$role)
            $roleList = strtolower($role);

        foreach ($roles as $role) {
            if (in_array($roles, $roleList)) {
                return true;
            }
        }

        return false;
    }
    
    /**
     * Set the super privillege role
     * 
     * @return string 
     */
    public function superRole()
    {
        $super = config('roles.super');

        if (is_array($super)) {
            $super = head($super);
        }

        return (string) strtolower($super);
    }

}
