<?php

namespace App\Http\Middleware;

use Closure;
use App\IpList;
use Symfony\Component\HttpFoundation\IpUtils as CheckIp;

class BlackWhiteList
{

    /**
     * Predefined IP range list from cloudfare
     * 
     * @see https://www.cloudflare.com/ips-v4
     * @var $range
     */
    protected $range = [
                          '103.21.244.0/22',
                          '103.22.200.0/22',
                          '103.31.4.0/22',
                          '104.16.0.0/12',
                          '108.162.192.0/18',
                          '131.0.72.0/22',
                          '141.101.64.0/18',
                          '162.158.0.0/15',
                          '172.64.0.0/13',
                          '173.245.48.0/20',
                          '188.114.96.0/20',
                          '190.93.240.0/20',
                          '197.234.240.0/22',
                          '198.41.128.0/17',
                          '199.27.128.0/21',
                         ];

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if ($this->isBlacklisted($request)) {
            return view('errors.403');
        }

        return $next($request);
    }

    /**
     * Check for current request (ip) from being blacklisted
     * 
     * @param  \Illuminate\Http\Request  $request
     * @return  bool  Request Session: 'blackwhitelist'
     */
    public function isBlacklisted($request)
    { 
        $ip = $request->ip();
        $session = $request->session();

        //First we check for request session 'blackwhitelist'
        //existance. If it does, return its value. This check
        //is good to prevent from opening database request on
        //every page load
        if ($session->has('blackwhitelist')) {
            return $session->get('blackwhitelist');
        }

        //If session doesn't exists. Set default value to
        //returned value of checkForBlacklist()
        return $session->get(
               'blackwhitelist',
               $this->checkForBlacklist($ip)
        );
    }

    /**
     * Check if current requested IP address is blacklisted
     * 
     * @param  string  $ip  requested ip address
     * @return bool
     */
    public function checkForBlacklist($ip)
    {

        //Check current IP if it's blacklisted
        //from predefined list
        if ($this->defaultCheck($ip)) {
            return true;
        }

        //Check current IP from database
        if ($this->databaseCheck($ip)) {
            return true;
        }

        return false;
    }

    /**
     * Check IP from predefined list
     * 
     * @param  string  $ip  requested ip address
     * @return bool
     */
    public function defaultCheck($ip)
    {
        return in_array($ip, $this->range);
    }

    /**
     * Check IP from database
     * 
     * @param  string  $ip  requested ip address
     * @return bool
     */
    public function databaseCheck($ip)
    {
        //Override if is on whitelist
        if ($this->isOnWhiteList($ip)) {
            return false;
        }

        $addresses = IpList::blacklist();

        foreach ($addresses as $address) {
          if (CheckIp::checkIp($ip, $address->address)) {
            return true;
          }
        }

        return false;
    }

    /**
     * Check current IP if whitelisted
     * 
     * @param  string  $ip  Request::ip()
     * @return bool
     */
    public function isOnWhitelist($ip) {
        $blacklist = IpList::isBlacklisted($ip);

        return !is_null($blacklist) ? !$blacklist : false;
    }

}
