<?php

namespace App\Http\Controllers\User;

use Route;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;

class ProfileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        //$this->middleware(['auth', 'rrr']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $username)
    {
        //dump(Route::current()->getParameter('username'));
        //dump($username);
        return view('user.profile');
    }
}
