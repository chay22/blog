<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tag;
use App\Http\Requests;

class TagController extends Controller
{
    public function blank(Request $request)
    {
        if ($request->ajax()) {
            $tags = Tag::get(['id', 'name']);

            foreach ($tags as $name) {
                $tag[] = $name->name;
            }

            return response()->json($tag);
        }

        return redirect()->route('blog');
    }

    public function index(Request $request, $tag)
    {
        $tags = Tag::where('slug', $tag)->with('posts')->get();

        return view('blogs.tag', [
            'tag' => $tags[0],
            'posts' => $tags[0]->posts
        ]);
    } 
}
