<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tag;
use App\Post;
use Auth;
use Carbon\Carbon;
use App\Http\Requests;
use App\Http\Requests\BlogPostCreationRequest;


class BlogController extends Controller
{
    public $posts;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Show blog index.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $posts = Post::whereNotNull('published_at')->orderBy('created_at', 'desc')->get();

        return view('blogs.index', ['posts' => $posts]);
    }

    /**
     * Show single post.
     *
     * @return \Illuminate\Http\Response
     */
    public function post(Request $request, $slug)
    {
        $post = Post::whereNotNull('published_at')
                    ->where('slug', $slug)
                    ->with('series')
                    ->firstOrFail();

        $id = $post->id;
        $tags = $post->tags;

        $related = $post->load(['tags' => function ($query) use ($tags) {
            $query->where('tag_id', $tags[0]->id);
        }])->where('id', '<>', $id)->take(4)->get();

        return view('blogs.post', [
            'post' => $post,
            'related' => $related
        ]);
    }

    /**
     * Proceed creating new blog post.
     *
     * @return \Illuminate\Http\Response
     */
    public function createNewPost(Request $request)
    {
        if (! $request->user()) {
            return redirect()->route('blog');
        }

        return view('blogs.create');
    }

    /**
     * Proceed creating new blog post.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(BlogPostCreationRequest $request)
    {
        abort_unless($request->ajax(), 422);

        $file = $request->file('image');
        $hash = str_random(5) . Carbon::now()->format('U');
        $image = 'chay-' . $hash . '-' . $file->getClientOriginalName();
        $file->move(public_path().'/images/', $image);
        
        $post = new Post();
        $post->title = $request->title;
        $post->slug = $request->slug ?: str_slug($request->title, '-');
        $post->excerpt = $request->excerpt;
        $post->content = $request->content;
        $post->image = $image;
        $post->user_id = 1;
        $post->published_at = Carbon::now();
        $save = $post->save();
        
        abort_unless($save, 500);

        $post->tags()->attach($request->tag);

        return response()->json([
            'status' => 200,
            'message' => 'Successfully created new post.',
            'path' => route('blog.post', ['slug' => $post->slug])
        ]);
    }
}
