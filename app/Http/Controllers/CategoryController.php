<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Route;
use App\Http\Requests;

class CategoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Route $route, Request $request)
    {
    	//$this->middleware('rrr');
    	$param = strtolower($route::currentRouteName());
    	$value = strtolower($request->route($param));
    	
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($category)
    {
    	//dump($category);
        return view('home');
    }

    /**
     * 
     * 
     * 
     * 
     */
    public function validateCurrentRoute($category)
    {
         $class = class_basename($this);
         $controller = class_basename(parent::class);
         $current = explode($controller, $class);
         $current = strtolower(head($current));

         return $current === $category;
    }


}
