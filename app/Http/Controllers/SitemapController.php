<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use URL;
use Carbon\Carbon;
use Watson\Sitemap\Sitemap;
use App\Post;
use App\Http\Requests;

class SitemapController extends Controller
{
    /**
     * @var \Watson\Sitemap
     */
    protected $sitemap;

    /**
     * Inject dependencies.
     *
     * @param  \Watson\Sitemap $sitemap
     */
    public function __construct(Sitemap $sitemap)
    {
        $this->sitemap = $sitemap;
    }

    /**
     * Show Sitemap index.
     * 
     * @return string XML type file
     */
    public function index()
    {
        $this->sitemap->addSitemap(route('sitemap.main'), Carbon::now());
        $this->sitemap->addSitemap(route('sitemap.blog'), Carbon::now());
        
        return $this->sitemap->index();
    }

    /**
     * Show Sitemap
     * 
     * @return string XML type file
     */
    public function main()
    {
        $this->sitemap->addTag(URL::to('/'), Carbon::now()->format('Y-m-d'), 'daily', '1.0');
        $this->sitemap->addTag(URL::to('todo'), Carbon::now()->format('Y-m-d'), 'weekly', '0.8');
        $this->sitemap->addTag(URL::to('sitemap'), Carbon::now()->format('Y-m-d'), 'daily', '0.5');
        $this->sitemap->addTag(URL::to('stuff'), Carbon::now()->format('Y-m-d'), 'weekly', '0.7');
        $this->sitemap->addTag(URL::to('blog'), Carbon::now()->format('Y-m-d'), 'daily', '1.0');
        
        return $this->sitemap->render();
    }

    /**
     * Sitemap from blog posts
     *
     * @return string XML type file
     */
    public function blog()
    {
        $posts = Post::whereNotNull('published_at')->get();

        foreach ($posts as $post) {
            $this->sitemap->addTag(route('blog.post', $post->slug), $post->updated_at, 'weekly', '0.9');
        }

        return $this->sitemap->render();
    }
}
