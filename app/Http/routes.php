<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*
|--------------------------------------------------------------------------
| Homepage
|--------------------------------------------------------------------------
*/
Route::get('/', 'HomeController@index');
Route::post('/', 'HomeController@sendMail');

/*
|--------------------------------------------------------------------------
| Todo page
|--------------------------------------------------------------------------
*/
Route::resource('todo', 'TodoController');


/*
|--------------------------------------------------------------------------
| My Stuff page
|--------------------------------------------------------------------------
*/
Route::resource('stuff', 'MyStuffController');


/*
|--------------------------------------------------------------------------
| Blog page
|--------------------------------------------------------------------------
*/
Route::get('blog/create', 'BlogController@createNewPost');
Route::post('blog/create', 'BlogController@create');
Route::get('blog', 'BlogController@index')->name('blog');
Route::get('blog/{slug}', 'BlogController@post')->name('blog.post');


/*
|--------------------------------------------------------------------------
| Tag page
|--------------------------------------------------------------------------
*/
Route::get('tag', 'TagController@blank');
Route::get('tag/{tag}', 'TagController@index')->name('tag');


/*
|--------------------------------------------------------------------------
| Authentication (Change this!!!)
|--------------------------------------------------------------------------
*/
Route::get('login', 'Auth\AuthController@showLoginForm');
Route::post('login', 'Auth\AuthController@login');
Route::get('logout', 'Auth\AuthController@logout');

// Registration Routes...
//Route::get('register', 'Auth\AuthController@showRegistrationForm');
//Route::post('register', 'Auth\AuthController@register');

// Password Reset Routes...
//Route::get('password/reset/{token?}', 'Auth\PasswordController@showResetForm');
//Route::post('password/email', 'Auth\PasswordController@sendResetLinkEmail');
//Route::post('password/reset', 'Auth\PasswordController@reset');

/*
|--------------------------------------------------------------------------
| User Settings ||| Authenticated
|--------------------------------------------------------------------------
*/
//Route::group([
//			  'middleware' => 'auth',
//			  'namespace' => 'User',
//	], function() {
//
//		Route::get('settings', 'SettingsController@index')
//			  ->name('settings');
//
//		Route::get('change-password', 'SettingsController@showChangePasswordPage');
//
//		Route::post('change-password', 'SettingsController@changePassword')
//			 ->name('change-password.update');
//});


/*
|--------------------------------------------------------------------------
| User Profile
|--------------------------------------------------------------------------
*/
//Route::get('@{username}', 'ProfileController@index')
//	 ->name('profile');

/*
|--------------------------------------------------------------------------
| Sitemap
|--------------------------------------------------------------------------
*/
Route::get('sitemap', 'SitemapController@index')->name('sitemap');
Route::get('sitemap/main', 'SitemapController@main')->name('sitemap.main');
Route::get('sitemap/blog', 'SitemapController@blog')->name('sitemap.blog');




