<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    /*
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'slug',
    ];

    /**
     * Apply relationship
     *
     * @return collection
     */
    public function posts()
    {
        return $this->belongsToMany('App\Post')->orderBy('published_at');
    }
}
