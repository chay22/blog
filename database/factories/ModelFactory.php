<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/
/*
$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'username' => $faker->userName,
        'email' => $faker->email,
        'password' => bcrypt('Abcd1234'),
        'website' => $faker->domainName,
        'gender' => 'female',
        'location' => $faker->streetAddress,
        'remember_token' => str_random(10),
        'role' => 'user',
    ];
});

$factory->define(App\IpList::class, function (Faker\Generator $faker) {
    return [
        'address' => $faker->ipv4,
        'blacklist' => true,
    ];
});

$factory->define(App\TodoCategory::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->domainWord,
    ];
});


$factory->define(App\Todo::class, function (Faker\Generator $faker) {
    return [
        'task' => $faker->text(200),
    ];
});

