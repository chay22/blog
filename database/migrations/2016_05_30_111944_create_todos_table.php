<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTodosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('todos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('task');
            $table->string('status')->default('progress');
            $table->integer('todo_category_id')
                  ->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('todo_category_id')
                  ->references('id')->on('todo_categories')
                  ->onUpdate('cascade')
                  ->onDelete('set null');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('todos');
    }
}
