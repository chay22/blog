<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function ($table) {
            $table->bigIncrements('id')->unsigned();
            $table->string('title', 100);
            $table->string('slug', 120)->unique();
            $table->string('image')->nullable();
            $table->text('excerpt');
            $table->longtext('content');
            $table->integer('user_id')->unsigned()->index();
            $table->foreign('user_id')->references('id')->on('users');
            $table->integer('series_id')->unsigned()->nullable()->index();
            $table->foreign('series_id')->references('id')->on('series');
            $table->timestamp('published_at')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        DB::statement('ALTER TABLE posts ADD FULLTEXT blogpost(title, content)');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('posts');
    }
}
