var Ready = require("domready");
var Vue = require('vue')
var Greets = require('./components/Greeting.vue');
var ToggleMenu = require('./components/ToggleMenu.vue');
var GetMenu = require('./modules/Menu.js');
var ScrollAnimate = require('./modules/ScrollAnimate.js');
var Modal = require('./modules/Modal.js');

import Markdown from './components/Markdown.vue'
import VueResource from 'vue-resource'

Vue.use(VueResource);
var token_id = document.querySelector('#_token');
var token = token_id ? token_id.getAttribute('content') : 'string';

Vue.http.headers.common['X-CSRF-TOKEN'] = token;
Vue.config.debug = true;

new Vue({
  el: 'body',
  ready: function() {
  		GetMenu.active()
  },
  components: { ToggleMenu, Greets, Markdown }
});



Ready(function() {
    ScrollAnimate();
    Modal.attach();
});