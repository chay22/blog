(function() {
  var Carousel;

  module.exports = Carousel = (function() {
    function Carousel(sel) {
        var selector = sel || '#tagList';
        this.slides = document.querySelectorAll("" + selector + " > *");
        this.slide_total = this.slides.length;
        this.slide_current = 1;
        this.slide_start = 1;
        this.slide_margin_left = parseInt(window.getComputedStyle(this.slides[0], null).getPropertyValue('margin-left'), 10);
        var parentWidth = document.querySelector(selector).offsetWidth;
        this.slide_width = this.slides[0].offsetWidth + this.slide_margin_left;

        var addEvent = function(object, type, callback) {
            if (object == null || typeof(object) == 'undefined') return;
            if (object.addEventListener) {
                object.addEventListener(type, callback, false);
            } else if (object.attachEvent) {
                object.attachEvent("on" + type, callback);
            } else {
                object["on"+type] = callback;
            }
        };
/*
        var w = window,
            d = document,
            e = d.documentElement,
            g = d.getElementsByTagName('body')[0],
            windowWidth = w.innerWidth || e.clientWidth || g.clientWidth;

        if (windowWidth >= 960) {
            this.slide_start = 6;
        } else {
            this.slide_start = 1;
        }

        var that = this;

        addEvent(window, 'resize', function(event) {
            var width = (event.srcElement || event.currentTarget).innerWidth;
            if (width >= 960) {
                that.slide_start = 6;
            } else {
                that.slide_start = 1;
            }
            console.log(this);
            console.log(Carousel);
        });
*/
        var that = this;
        var next = document.querySelector('.next');
        var prev = document.querySelector('.prev');


        addEvent(next, 'click', function(event) {
            return that.next();
        });

        addEvent(prev, 'click', function(event) {
            return that.prev();
        });

    }

    Carousel.prototype.next = function() {
        console.log(this)
        var total = this.slide_current + this.slide_start;

        if (total > this.slide_total) {
            this.slide_current = this.slide_total;
            return;
        }

        var margin = this.slide_current * this.slide_width;

        this.slides[0].style.marginLeft = (margin * -1)+'px';


        this.slide_current += 1;
    };

    Carousel.prototype.prev = function() {
      console.log(this);
        if (this.slide_current <= 0) {
            this.slide_current = 0;
            return;
        }

        this.slides[0].style.marginLeft = this.slide_margin_left;

        this.slide_current -= 1;
    };

    return Carousel;

  })();

}).call(this);
