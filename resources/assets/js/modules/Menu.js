var Menu = {
    
    active: function() {
        var menus = document.querySelectorAll('#menu > li > a'),
            url = window.location.pathname;

        for (let menu of menus) {
        	if (Object.prototype.hasOwnProperty.call(menus,menu)) {
        		if (menu.getAttribute('href') === url) {
        			menu.className += ' active';
                }
        	}
        } 
    }
}

module.exports = Menu;