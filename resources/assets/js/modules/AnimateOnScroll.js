var scrollMonitor = require('scrollMonitor');
var AnimationFrame = require('animation-frame');
var Animate = new AnimationFrame(25)

var AnimateOnScroll = {
    //Store objects of scrollMonitor from specific selector here
    selector: [],

    //Get object of scrollMonitor from specified selector
    sel: function (sel = null){
        var selector = this.selector,
            node = document.querySelector(sel),
            el

        for (var i = 0; i < selector.length; i++) {
            if (selector[i]['watchItem'] === node) {
                el = selector[i]
            }
        }

        if (el.length == 0) { 
            throw new Error('AnimateOnScroll: Please attach the selector(s) first')
        }

        return el
    },

    //Create new scrollMonitor object then store it on selector property
    attach: function(sel) {

        sel = this._validate(sel)
        var selector = this.selector
        var _el = [], _sel = []
        //Store scrollMonitor object to this.selector
        for (var i = 0; i < sel.length; i++) {
             _el[i] = document.querySelectorAll(sel[i]);
        }
        
        _el.map(
            val => {
                var len = val.length
                for (var s = 0; s < len; s++) {
                    _sel.push(val[s])
                }
            }
        )

        for (var s = 0; s < _sel.length; s++) {
            selector[s] = scrollMonitor.create(_sel[s])
        }

        return this
    },
    allOn: function(event, callback, isOne = false) {
        var selector = this.selector, len = selector.length

        var type = Object.prototype.toString.call(callback)
        if (typeof callback === 'undefined' && type !== '[object Function]') {
            throw new Error(
            'AnimationOnScroll: Please provide a function callback on allOn() method'
            )
        }

        for (var i = 0; i < len; i++) {
            selector[i].on(event, callback, isOne)
        }
    },
    animate: function(sel){
                Animate.request(function(){
                    sel.style.transform = 'translate3d(-100%, 0, 0)'
                })

    },
    _validate: function(sel) {
        var type = Object.prototype.toString.call(sel)

        if (type !== '[object Array]') {
            //Allow selector to be string for single selector
            if (type === '[object String]') {
                //turn it out to array for iteration
                return [sel]
            } else throw new Error(
'AnimateOnScroll: Selector(s) must be either type of string (for single selector) or array'
            )
        }

        return sel
    }
}


module.exports = AnimateOnScroll