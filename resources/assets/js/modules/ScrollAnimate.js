/**
 * Dependencies
 *
 * @package scrollMagic
 * @plugin  debug.addIndicators || scrollMagic plugin (copy this first)
 */

var ScrollMagic = require('scrollmagic')
//require('../../../../node_modules/scrollmagic/scrollmagic/uncompressed/plugins/debug.addIndicators')

var ScrollAnimate = function(){
    var app = ScrollAnimate.init(),
        //data = ScrollAnimate.animate(),
        intro = ScrollAnimate.animate('#introduction', 'onLeave')
        //container = ScrollAnimate.animate('.container-content', 'onLeave')
    
    //return if selector not found
    if (! intro) return;

    var scene = [
        intro
    ]
    
    app.addScene(scene)

    //data.duration('100%')
    //data.on('enter', function(e){
        //var op = e.target.controller().info('scrollDirection')
        //console.log(op)
    //}).on('leave', function(e){
        //this.destroy()
        //console.log(e)
    //}).on('progress', function(e) {
        //var op = e.progress.toFixed(2)
        //this.selector.style.opacity = op
    //})

    intro.setPin(intro.selector, {pushFollowers: false})
    intro.duration(500)
    intro.on('progress', function(e){
        var prog = e.progress.toFixed(2);
        this.selector.style.opacity = 1 - parseFloat(prog);
    })
    
   

}

/**
 * Configuration
 *
 * MAINELEMENT      scrollMagic controller element
 * ELEMENT          scrollMagic default scene element
 * DEBUG            scrollMagic debuglevel
 * REFRESHINTERVAL  scrollMagic scene refreshInterval
 */
ScrollAnimate.config = {
    MAINELEMENT     : 'body',
    ELEMENT         : '[data-scroll-animate]',
    DEBUG           : 0,
    REFRESHINTERVAL : 0
}

/**
 * Scroll Magic controller wrapper
 *
 * @return  instance of Scroll Magic controller
 */
ScrollAnimate.init = function() {
    var config = ScrollAnimate.config
    return new ScrollMagic.Controller({
        container: config.MAINELEMENT,
        loglevel: config.DEBUG,
        refreshInterval: config.REFRESHINTERVAL
    })
}

/**
 * Scroll Magic scene wrapper
 *
 * @param   string||null  el  scene element (optional)
 * @param   string      hook  scene hook (optional)
 * @return  instance of Scroll Magic scene
 */
ScrollAnimate.animate = function(el,hook = 'onCenter') {
    var config = ScrollAnimate.config

    if (typeof el === 'undefined') el = config.ELEMENT

    var scene = new ScrollMagic.Scene({
        triggerElement: el,
        triggerHook: hook,
        loglevel: config.DEBUG
    })
    var selector = document.querySelector(el);

    //return false if no selector found
    if (selector === null) return false;

    scene['selector'] = selector;

    return scene;
}

/*
|--------------------------------
| Export the variable
|--------------------------------
*/
module.exports = ScrollAnimate