var Modal = (function(){

    var hasClass = function(el, cls) {
        return (' ' + el.className + ' ').indexOf(' ' + cls + ' ') > -1;
    }

    var trigger = function(callback) {
        var triggers = document.querySelectorAll('[data-toggle="modal"]');
        var modal;
        for (var i = 0; i < triggers.length; i++) {
            modal = triggers[i].getAttribute('href') || triggers[i].getAttribute('data-target');
            triggers[i].addEventListener('click', function (e) {
                e.preventDefault();
                var target = document.querySelector('.modal' + modal);
                if (target === null || !target) {
                    throw new Error('You need to add ID "' + modal + '"" to modal container!');
                }
                target.className += ' active';
            });
        }
    }

    var overlay = function() {
        var overlay = document.querySelector('.modal-overlay');
        overlay.addEventListener('click', function (e) {
            e.preventDefault();
            var parent = e.currentTarget.parentElement;
            if (hasClass(parent, 'modal')) {
                if (! hasClass(e.currentTarget, 'locked')) {
                    parent.className = 'modal';
                }
            }
        });
    }

    var closeButton = function() {
        var modal = document.querySelectorAll('.modal');
        var id, btn;
        for (var i = 0; i < modal.length; i++) {
            var mdl = modal[i];
            btn = modal[i].querySelector('.modal-header > .btn-clear');
            btn.addEventListener('click', function (e) {
                e.preventDefault();
                mdl.className = 'modal';
            });
        }
    }

    return {
        attach: function() {
            trigger();
            overlay();
            closeButton();
        }
    }

}())

module.exports = Modal;