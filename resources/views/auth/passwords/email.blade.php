@extends('layouts.auth')

@section('content')
<header class="navbar navbar-default">
    <div class="navbar-header">
        <a href="/" class="navbar-brand">Chay</a>
    </div>
    <div class="navbar-section">
        <a href="/login">Login</a>
    </div>
</header>

<div id="app" class="auth content">
    <main id="login" class="login">
        <section class="col-10 col-xs-9 col-sm-6 col-md-5 col-lg-4 col-xl-3 centered">
            @if (session('status'))
            <div class="toast toast-danger">
                <strong>{{ session('status') }}</strong>
            </div>
            @endif
            <form role="form" method="POST" action="{{ url('/password/email') }}">
                {{ csrf_field() }}

                <div class="form-group{{ $errors->has('email') ? ' has-danger' : '' }}">
                    <label class="form-label" for="Email">Email</label>
                    <input class="form-input" id="Email" type="email" name="email" value="{{ old('email') }}" />
                </div>

                @if ($errors->has('email'))
                <div class="toast toast-danger">
                    <strong>{{ $errors->first('email') }}</strong>
                </div>
                @endif

                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-blocking" role="button">Send Password Reset Link</button>
                </div>

            </form>
        </section>
    </main>
</div>
@endsection

