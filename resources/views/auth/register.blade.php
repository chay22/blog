@extends('layouts.auth')

@section('content')
<header class="navbar navbar-default">
    <div class="navbar-header">
        <a href="/" class="navbar-brand">Chay</a>
    </div>
    <div class="navbar-section">
        <a href="/login">Have an account? Login.</a>
    </div>
</header>

<div id="app" class="auth content">
    <main id="login" class="login">
        <section class="col-10 col-xs-9 col-sm-6 col-md-5 col-lg-4 col-xl-3 centered">
            <form role="form" method="POST" action="{{ url('/register') }}">
                {{ csrf_field() }}

                <div class="form-group{{ $errors->has('username') ? ' has-danger' : '' }}">
                    <label class="form-label" for="Username">Username</label>
                    <input class="form-input" id="Username" type="text" name="username" value="{{ old('username') }}" />
                </div>

                @if ($errors->has('username'))
                <div class="toast toast-danger">
                    <strong>{{ $errors->first('username') }}</strong>
                </div>
                @endif

                <div class="form-group{{ $errors->has('email') ? ' has-danger' : '' }}">
                    <label class="form-label" for="Email">Email</label>
                    <input class="form-input" id="Email" type="email" name="email" value="{{ old('email') }}" />
                </div>

                @if ($errors->has('email'))
                <div class="toast toast-danger">
                    <strong>{{ $errors->first('email') }}</strong>
                </div>
                @endif

                <div class="form-group{{ $errors->has('password') ? ' has-danger' : '' }}">
                    <label class="form-label" for="Password">Password</label>
                    <input class="form-input" id="Password" type="password" name="password" value="{{ old('password') }}" />
                </div>

                @if ($errors->has('password'))
                <div class="toast toast-danger">
                    <strong>{{ $errors->first('password') }}</strong>
                </div>
                @endif

                <div class="form-group{{ $errors->has('password_confirmation') ? ' has-danger' : '' }}">
                    <label class="form-label" for="ConfirmPassword">Confirm Password</label>
                    <input class="form-input" id="ConfirmPassword" type="password" name="password_confirmation" value="{{ old('password_confirmation') }}" />
                </div>

                @if ($errors->has('password_confirmation'))
                <div class="toast toast-danger">
                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                </div>
                @endif
                
                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-blocking" role="button">Register</button>
                </div>
            </form>
        </section>
    </main>
</div>
@endsection

