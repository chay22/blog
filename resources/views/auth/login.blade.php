@extends('layouts.auth')

@section('content')
<header class="navbar navbar-default">
    <div class="navbar-header">
        <a href="/" class="navbar-brand">Chay</a>
    </div>
    <!--<div class="navbar-section">
        <a href="/register">Register an account</a>
    </div>-->
</header>

<div id="app" class="auth content">
    <main id="login" class="login">
        <section class="col-10 col-xs-9 col-sm-6 col-md-5 col-lg-4 col-xl-3 centered">
            <form name="login" method="POST" action="{{ url('/login') }}">
                {{ csrf_field() }}

                <div class="form-group{{ $errors->has('login') ? ' has-danger' : '' }}">
                    <label class="form-label" for="Username">Username</label>
                    <input class="form-input" id="Username" type="text" name="login" value="{{ old('login') }}" />
                </div>

                @if ($errors->has('login'))
                <div class="toast toast-danger">
                    <strong>{{ $errors->first('login') }}</strong>
                </div>
                @endif

                <div class="form-group{{ $errors->has('password') ? ' has-danger' : '' }}">
                    <label class="form-label" for="Password">Password</label>
                    <input class="form-input" id="Password" type="password" name="password" value="{{ old('password') }}" />
                </div>

                @if ($errors->has('password'))
                <div class="toast toast-danger">
                    <strong>{{ $errors->first('password') }}</strong>
                </div>
                @endif
                

                <div class="form-group">
                    <label class="form-checkbox">
                        <input type="checkbox" name="remember" />
                        <i class="form-icon"></i>  Remember me
                    </label>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-blocking" role="button">Login</button>
                </div>
                
                <div class="form-group text-center">
                    <a class="btn btn-link" href="{{ url('/password/reset') }}">Forgot Your Password?</a>
                </div>

            </form>
        </section>
    </main>
</div>
@endsection

