@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>
                <h2> {{ Auth::user()->username }} </h2>
                <div class="panel-body">
                    Settings 
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
