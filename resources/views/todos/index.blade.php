@extends('layouts.app')

@section('title', 'My Todo List')

@section('content')
<main id="todo" class="todo"> 
    <h1 class="header"><span class="omi omi-todo"></span> TODO</h1>
    <section class="content">
        <div class="block-separator"></div>
        @if (count($todos) > 0)
        <table class="table table-todo table-hover col-xs-11 col-sm-9 center">
            <tbody>
                @foreach ($todos as $todo)
                    <tr class="{{ !$todo->strike ?: 'strike' }}">
                        <td>{{ $todo->created_at }}</td>
                        <td>{{ $todo->task }}</td>
                        <td>
                            {{ $todo->status }}
                            @if (! is_null($todo->updated_at))
                                <i class="omi omi-check tooltip tooltip-top" data-tooltip="at: {{ $todo->updated_at }}"></i>
                            @endif
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        @else
        <div class="empty col-xs-11 col-sm-9 center">
            <i class="omi omi-todo-alt"></i>
            <p class="empty-title">It's shame, but yes, I have nothing to do right now.</p>
            <p class="empty-meta">Please take a look at my <a href="/blog" title="blog">blog</a> to see what I'm currently planning.</p>
        </div>
        @endif
    </section>
</main>
@endsection
