@extends('layouts.app')

@section('title', 'A Learning Learner')

@section('content')
<main class="home" role="main">
    <section id="introduction" class="header content">
        <div class="column center col-12 col-xs-10 col-md-8 col-lg-6">
            <div class="column col-xs-4 greeting">
                <greets class="text"></greets>
            </div>
            <div class="card">
                <div class="card-body">
                    <p itemscope itemtype="http://schema.org/Person">
                        My name is <span itemprop="name">Cahyadi Nugraha</span> you can simply call me
                        <span class="highlight text-bold" itemprop="alternateName">Chay</span>. I had tried
                        to learn web programming a bit in last couple years and start to
                        passionate in this field since early this year. Let me introduce
                        myself a little longer, just scroll down the page.
                    </p>
                </div>
            </div>
        </div>

        <div id="scrollDown" class="scroll-down center">
            <i class="omi omi-go-down"></i>
        </div>
    </section>
 
 <div class="container-content" itemprop="mainContentOfPage">   
    <section id="likedThings" class="container">
        <h2>THINGS I LIKED MOST</h2>
        <p>
           To avoid a blessing in disguise, you'd better know me further
        </p>
        <div class="columns col-12 col-xs-10 col-md-12 col-lg-10 center">
            <div class="column col-12 col-md-4">
                <i class="omi omi-yoga"></i>
                <h3>Peace, Serenity</h3>
                <p>
                    I can't resist that I like to live in a place with no crowd.
                </p>
                <p>
                    Minified my foul minded thinking, arrogance, misery and else 
                    meanness human-being job – will do. In, of course, a serenity.
                </p>
            </div>
            <div class="column col-12 col-md-4">
                <i class="omi omi-guitar"></i>
                <h3>Guitar and Music</h3>
                <p>
                    Though I only play guitar for myself, sometimes, I hate it when one stops me
                    while already playing it. Even if I did it for hours already.
                </p>
                <p>
                    Increasing such creativity is also an important thing to me, just to make
                    sure I'll always have a good-sharp logical thinking.
                </p>
            </div>
            <div class="column col-12 col-md-4">
                <i class="omi omi-screen"></i>
                <h3>Computer and Internet</h3>
                <p>
                    Science and technology are always a demand. I will never
                    ever let myself taking no advantage around those atmospheres. Yes, I like to learn
                    something new. As seen, I'm a self-taught all the way. 
                </p>
            </div>
        </div>
    </section>
    
    <div class="clarify center text-center">
        <span>Okay, enough saying crappy things.</span>
        <p>
            Let's talk about what does this site is made for; what do I do. Keep scrolling down.
        </p>
    </div>

    <section id="todo" class="container">
        <h2>A Todo List</h2>
        <div class="columns col-9 col-md-11 col-lg-10 center">
            <div class="column col-md-3 col-lg-2 text-center">
                <i class="omi omi-todo"></i>
            </div>
            <div class="column col-md-9 col-lg-10">
                <p>
                    Yes! This site, particularly, is created to show my progress in learning
                    (specific) programming languages. What I've done and what I am going to achieve.
                    Every single <em>todo</em> I have published will be used as a reminder
                    about things I have learn before.

                    Apart from that, there will be, of course some stuffs I'll show in here:
                    <a href="/stuff" title="My Stuffs">Stuffs</a> — as a result from learning on 
                    specific libraries/frameworks/languages. Also, I'll try to write some blog
                    posts about how I accomplish that.
                </p>
            </div>
        </div>

        <div class="center column text-center">
            <a href="/todo" class="btn btn-blue col-9 col-xs-7 col-sm-6 col-md-5 col-lg-3" title="Todo">Visit My Todo List</a>
        </div>
    </section>

    <section id="focus" class="container">
        <h2>MAIN FOCUSES</h2>
        <p>By learning on "how to use" a framework/library.</p>
        <div class="column focus-box col-11 col-lg-9 center">
            <div class="columns first-box" itemscope itemtype="http://schema.org/SoftwareSourceCode">
                
                <div class="column ide-box col-lg-6">
                    <div class="ide center">
                        <div class="ide-titlebar">
                            <span class="ide-buttons">
                                <span class="ide-button ide-button-full"></span>
                                <span class="ide-button ide-button-hide"></span>
                                <span class="ide-button ide-button-close"></span>
                            </span>
                            <span class="ide-title"> \www\project\App\What.php</span>
                        </div>
                        <div class="ide-content">
                            <code class="code" itemprop="codeSampleType">
                                <span class="line">{{{ '<'.'?php' }}}</span>
                                <span class="line"><span class="namespace">namespace App</span>;</span>
                                <span class="line"></span>
                                <span class="line"><span class="keyword">use</span> <span class="namespace">ArrayObject</span>;</span>
                                <span class="line"></span>
                                <span class="line"><span class="class">class</span> What</span>
                                <span class="line">{</span>
                                <span class="line comment">&nbsp;&nbsp;&nbsp;&nbsp;/**</span>
                                <span class="line comment">&nbsp;&nbsp;&nbsp;&nbsp; *&nbsp;&nbsp;Begin mutating string(s)</span>
                                <span class="line comment">&nbsp;&nbsp;&nbsp;&nbsp; *&nbsp;&nbsp;</span>
                                <span class="line comment">&nbsp;&nbsp;&nbsp;&nbsp; *&nbsp;&nbsp;<span class="keyword">@param</span>&nbsp;&nbsp;array&nbsp;&nbsp;$args</span>
                                <span class="line comment">&nbsp;&nbsp;&nbsp;&nbsp; *&nbsp;&nbsp;<span class="keyword">@return</span> class \ArrayObject</span>
                                <span class="line comment">&nbsp;&nbsp;&nbsp;&nbsp; */</span>
                                <span class="line">&nbsp;&nbsp;&nbsp;&nbsp;<span class="keyword">public function</span> <span class="function">justGo</span>(...$args)</span>
                                <span class="line">&nbsp;&nbsp;&nbsp;&nbsp;{</span>
                                <span class="line">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="keyword">return new</span> <span class="class">ArrayObject</span>($array);</span>
                                <span class="line">&nbsp;&nbsp;&nbsp;&nbsp;}</span>
                                <span class="line"></span>
                                <span class="line"></span>
                                <span class="line"></span>
                            </code>
                        </div>
                    </div>
                </div>
                <div class="column col-lg-6">
                    <div class="columns head">
                        <div class="column col-6 php">
                            <i class="omi omi-php"></i>
                        </div>
                        <div class="column col-6 laravel">
                            <i class="omi omi-laravel"></i>
                        </div>
                    </div>
                    <p class="column">
                        <abbr title="PHP: Hypertext Processor" itemprop="programmingLanguage">PHP</abbr>, for me, is 
                        the easiest language to learn when it comes to web programming. A non-programming background 
                        person should can make a site running into production without an effort with PHP. But sure, 
                        it doesn't make PHP known as a rubbish. Well, atleast for me. PHP does powerful thing as seen 
                        from PHP framework nowadays. Yes, I'm learning on one of them, Laravel.
                    </p>
                </div>

            </div>

            <hr>

            <div class="columns second-box">
                <div class="column col-md-2 head">
                    <i class="omi omi-mysql"></i>
                </div>
                <p class="column col-md-9" itemscope itemtype="http://schema.org/SoftwareSourceCode">
                    In every cases I usually use database to store everything. This why I'm also learning 
                    <abbr title="Structured Query Language" itemprop="programmingLanguage">MySQL</abbr> 
                    (it's Mariadb actually) after PHP. Anyways, often people who care so much about scaling 
                    won't mention MySQL at all. Even, "RDBMS' sucks, use NoSQL.", they said. Well, a thing 
                    differs each other by their purpose. I'll still learning MySQL until I done master it.
                </p>
            </div>
            
            <hr>

            <div class="columns third-box" itemscope itemtype="http://schema.org/SoftwareSourceCode">
                <div class="columns col-12 col-xs-8 col-sm-7 col-md-5 col-lg-2 head">
                    <div class="column javascript">
                        <i class="omi omi-js"></i>
                    </div>
                    <div class="column vue">
                        <i class="omi omi-vue"></i>
                    </div>
                </div>
                <div class="column col-lg-7 vertical-center">
                    <p>
                        To be a full package, I won't let myself leave the client-side part. Creating a fancy 
                        button, placing an understandable navigation are all my goal. Cover up my lack of 
                        designing skill – yes, CSS too – is why <span itemprop="programmingLanguage">Javascript</span> 
                        is also part of my things to learn. As of now, I fell in love with a somewhat called 
                        scalable lightweight Javascript framework, Vue.js.
                    </p>
                </div>
                <div class="column">
                    <div class="ide ide-showcase center">
                        <code class="code" itemprop="codeSampleType">
                            <span class="line"></span>
                            <span class="line"></span>
                            <span class="line"><span class="keyword">new</span> Vue({</span>
                            <span class="line">&nbsp;&nbsp;el: <span class="string">'#app'</span>,</span>
                            <span class="line">&nbsp;&nbsp;<span class="function">ready:</span> <span class="class">function</span>() {</span>
                            <span class="line">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Service.turnOn()</span>
                            <span class="line">&nbsp;&nbsp;},</span>
                            <span class="line">&nbsp;&nbsp;components: { Toggle }</span>
                            <span class="line">})</span>
                            <span class="line"></span>
                            <span class="line"></span>
                            <span class="line"></span>
                        </code>
                    </div>
                </div>
            </div>
        </div>
    </section>

</div>
</main>
@endsection
