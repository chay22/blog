@extends('layouts.app')

@section('title', 'My Useless Stuffs')

@section('content')
<main id="stuff" class="stuff"> 
    <h1 class="header"><span class="omi omi-books"></span> My Useless Stuffs</h1>
    <section class="content">
        <div class="block-separator"></div>
        <div class="columns col-xs-11 col-sm-9 center">
        @if (count($stuffs) > 0 )
            @foreach ($stuffs as $stuff)
            <div class="card text-center">
                <div class="card-header">
                    <h4 class="card-title">
                        <a href="{{ $stuff->url }}" title="{{ $stuff->name }}" target="_blank">
                            {{ $stuff->name }}
                        </a>
                    </h4>
                    <h6 class="card-meta">{{ $stuff->category }}</h6>
                </div>
                <div class="card-body tooltip tooltip-top" data-tooltip="{{ $stuff->description }}">
                    {{ strlen($stuff->description) > 50
                     ? str_finish(str_limit($stuff->description, 50), '...')
                     : $stuff->description
                    }}
                </div>
                <div class="card-footer">
                    <a href="{{ $stuff->url }}" class="btn btn-link tooltip tooltip-right" data-tooltip="Visit link" target="_blank">
                        <span class="omi omi-link"></span>
                    </a>
                </div>
            </div>
            @endforeach
        @else
            <div class="empty center">
                <i class="omi omi-books"></i>
                <p class="empty-title">Uhmm, I have nothing to serve at the moment.</p>
                <p class="empty-meta">Please take a look at my <a href="/blog" title="blog">blog</a> to see something else I'm creating.</p>
            </div>
        @endif
        </div>
    </section>
</main>
@endsection
