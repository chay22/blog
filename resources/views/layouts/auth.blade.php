<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>
    
    <!-- Styles -->
    <link href="{{ elixir('css/auth.css') }}" rel="stylesheet">

</head>
<body>
    @yield('content')

    <span class="col-12 back">Back to <a href="/">Homepage</a>.</span>

    <footer class="site-footer content">
        <div class="col-12 text-center bottom">
            <i class="omi omi-chay"></i>
            <span>
                Just a stuff by Cahyadi Nugraha
            </span>
        </div>
    </footer>
    <script src="{{ elixir('js/app.js') }}"></script>
</body>
</html>