<!DOCTYPE html>
<html lang="en" itemscope itemtype="http://schema.org/Blog">
<!--
█▀▀▀▀▀  █▀▀▀▀█  █    █  █     █  █▀▀▀▀█  █▀▀▀▀▄   █
█       █    █  █    █  █     █  █    █  █    █   █
█       █▀▀▀▀█  █▀▀▀▀█   ▀▀█▀▀   █▀▀▀▀█  █    █   █
█▄▄▄▄▄  █    █  █    █     █     █    █  █▄▄▄▄▀   █


█▄    █  █    █  ▄▀▀▀▀▀  █▀▀▀▀▄  █▀▀▀▀█  █    █  █▀▀▀▀█
█▀█▄  █  █    █  █       █    █  █    █  █    █  █    █
█  ▀█▄█  █    █  █  ▀▀█  █▀▀▀▀▄  █▀▀▀▀█  █▀▀▀▀█  █▀▀▀▀█
█    ▀█  ▀▄▄▄▄▀  ▀▄▄▄▄▀  █    █  █    █  █    █  █    █
-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" sizes="57x57" href="/apple-touch-icon-57x57.png?v=Kmm4p2v8QP">
    <link rel="apple-touch-icon" sizes="60x60" href="/apple-touch-icon-60x60.png?v=Kmm4p2v8QP">
    <link rel="apple-touch-icon" sizes="72x72" href="/apple-touch-icon-72x72.png?v=Kmm4p2v8QP">
    <link rel="apple-touch-icon" sizes="76x76" href="/apple-touch-icon-76x76.png?v=Kmm4p2v8QP">
    <link rel="apple-touch-icon" sizes="114x114" href="/apple-touch-icon-114x114.png?v=Kmm4p2v8QP">
    <link rel="apple-touch-icon" sizes="120x120" href="/apple-touch-icon-120x120.png?v=Kmm4p2v8QP">
    <link rel="apple-touch-icon" sizes="144x144" href="/apple-touch-icon-144x144.png?v=Kmm4p2v8QP">
    <link rel="apple-touch-icon" sizes="152x152" href="/apple-touch-icon-152x152.png?v=Kmm4p2v8QP">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon-180x180.png?v=Kmm4p2v8QP">
    <link rel="icon" type="image/png" href="/favicon-32x32.png?v=Kmm4p2v8QP" sizes="32x32">
    <link rel="icon" type="image/png" href="/android-chrome-192x192.png?v=Kmm4p2v8QP" sizes="192x192">
    <link rel="icon" type="image/png" href="/favicon-96x96.png?v=Kmm4p2v8QP" sizes="96x96">
    <link rel="icon" type="image/png" href="/favicon-16x16.png?v=Kmm4p2v8QP" sizes="16x16">
    <link rel="manifest" href="/manifest.json?v=Kmm4p2v8QP">
    <link rel="mask-icon" href="/safari-pinned-tab.svg?v=Kmm4p2v8QP" color="#34335c">
    <link rel="shortcut icon" href="/favicon.ico?v=Kmm4p2v8QP">
    <meta name="msapplication-TileColor" content="#2d89ef">
    <meta name="msapplication-TileImage" content="/mstile-144x144.png?v=Kmm4p2v8QP">
    <meta name="theme-color" content="#ffffff">

    <title>@hasSection('blog')
Blog - @yield('title')@else
Chay - @yield('title')@endif</title>
    
    <meta itemprop="name" content="Chay">
    <meta itemprop="creator" name="author" content="Cahyadi Nugraha">
    <meta name="description" content="A todo list, stuff, and everything else from a learning web developer.">
    <meta name="country" content="Indonesia">
    <meta property="og:title" content="Chay - @yield('title')">
    <meta property="og:site_name" content="Chay">
    <meta property="og:url" content="{{ url()->current() }}">
    <meta property="og:description" content="A todo list, stuff, and everything else from a learning web developer.">
    <meta property="og:type" content="profile">
    <meta name="twitter:card" content="summary">
    <meta name="twitter:title" content="Chay - @yield('title')">
    <meta name="twitter:description" content="A todo list, stuff, and everything else from a learning web developer.">
    <meta name="twitter:image:src" content="http://werron.dev/android-chrome-192x192.png">
    <link itemprop="url" rel="canonical" href="{{ url()->current() }}">

    <!-- Styles -->
    <link href="{{ elixir('css/app.css') }}" rel="stylesheet">
    
    <script type="application/ld+json">
    {
      "@context": "http://schema.org",
      "@type": "Organization",
      "url": "{{ url('/') }}",
      "name": "Chay",
      "logo": "{{ url('/') }}/android-chrome-192x192.png",
      "founder": [{
        "@type": "Person",
        "name": "Cahyadi Nugraha"
      }]
    }
    </script>
</head>
<body>
    <header class="navbar navbar-default">
        <div class="navbar-header">
            <toggle-menu class="btn show-sm"></toggle-menu>
            <a href="/" class="navbar-brand">Chay</a>
        </div>
        
        <nav class="navbar-section">
            <ul id="menu">
                <li><a href="{{ url('/') }}" class="btn">Home</a></li>
                <li><a href="{{ route('todo.index') }}" class="btn">Todos</a></li>
                <li><a href="{{ route('stuff.index') }}" class="btn">My Stuffs</a></li>
                <li><a href="{{ route('blog') }}" class="btn">Blog</a></li>
            </ul>
        </nav>
    </header>

    <div id="blog" class="content">
        @yield('content')
    </div>

    <div itemprop="breadcrumb" itemscope itemtype="http://schema.org/BreadCrumbList">
        <div itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
            <meta itemprop="position" content="1">
            <a href="{{ url('/') }}" itemprop="item">
                <meta itemprop="name" content="Home">
            </a>
        </div>
        <div itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
            <meta itemprop="position" content="2">
            <a href="{{ route('blog') }}" itemprop="item">
                <meta itemprop="name" content="Blog">
            </a>
        </div>
        <div itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
            <meta itemprop="position" content="2">
            <a href="{{ url()->current() }}" itemprop="item">
                <meta itemprop="name" content="@yield('title')">
            </a>
        </div>
    </div>
    <footer class="site-footer content">
        <div class="container">
            <div class="columns col-10 col-lg-11 center">

                <div class="columns center col-12 message">
                    <p class="column col-12 col-sm-4 col-lg-3 hey vertical-center">
                        Hey,
                    </p>
                    <p class="column col-12 col-sm-8 col-lg-9 vertical-center">
                        Wanna learn something together? Or handcraft some cool stuffs we could do?
                        Or even, have a project I could accomplish? Or just got a todo suggestion in mind for me?
                        Sure, we can talk almost about everything!
                    </p>
                </div>

                <div class="column col-md-12 col-lg-2 center contact" itemscope itemtype="https://schema.org/ContactPage">
                    <span class="get-in-touch">I'm here!</span>
                    <div class="col-8 col-xs-5 col-md-4 col-lg-12 center list">
                        <ul>
                            <li>
                                <a class="tooltip tooltip-right email" data-tooltip="cnu@protonmail.com" href="mailto:cnu@protonmail.com">
                                    <span class="omi omi-email"></span> Email
                                </a>
                            </li>
                            <li>
                                <a class="tooltip tooltip-right telegram" data-tooltip="@chay_nu" href="https://telegram.me/chay_nu">
                                    <span class="omi omi-telegram"></span> Telegram
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="column col-md-12 col-lg-2">
                    <div class="send vertical-center">
                        <small>or just, click here to</small>
                        <button class="btn btn-block send-email" data-target="#sendEmail" data-toggle="modal"><i class="omi omi-send-mail"></i> Send me a message</button>
                    </div>
                </div>

            </div>
        </div>

        <div class="column col-8 col-xs-5 center navbar-section">
            <a href="{{ url('/') }}">Home</a>
            <a href="{{ route('todo.index') }}">Todos</a>
            <a href="{{ route('stuff.index') }}">My Stuffs</a>
            <a href="{{ route('blog') }}">Blog</a>
            <a href="{{ url('/sitemap') }}">Sitemap</a>
        </div>

        <div class="col-12 text-center bottom">
            <i class="omi omi-chay"></i>
            <div class="copyright-text">
                <span itemprop="copyrightYear">2016</span> — 
                Just a stuff by <span itemprop="copyrightHolder">Cahyadi Nugraha</span>
            </div>
        </div>
    </footer>
    <!-- JavaScripts -->
    <script src="{{ elixir('js/app.js') }}"></script>

    <div id="sendEmail" class="modal">
        <div class="modal-overlay"></div>
        <div class="modal-container">
            <div class="modal-header">
                <button class="btn btn-clear float-right"></button>
            </div>
            <div class="modal-body">
                <div class="content col-8 center">
                    <form>
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label class="form-label" for="FakeName">Your fake name</label>
                            <input class="form-input" id="FakeName" type="text" name="fake_name" placeholder="Marco Polo" value="{{ old('fake_name') }}" />
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-danger' : '' }}">
                            <label class="form-label" for="Email">Your whatever email*</label>
                            <input class="form-input" id="Email" type="email" name="email" placeholder="repliable_mail@example.org" value="{{ old('email') }}" />
                        </div>

                        @if ($errors->has('email'))
                        <div class="toast toast-danger">
                            <strong>{{ $errors->first('email') }}</strong>
                        </div>
                        @endif

                        <div class="form-group">
                            <label class="form-label" for="Subject">Mail subject or something</label>
                            <input class="form-input" id="Subject" type="text" name="subject" placeholder="Thou shalt do dis!" value="{{ old('subject') }}" />
                        </div>

                        <div class="form-group{{ $errors->has('message') ? ' has-danger' : '' }}">
                            <label class="form-label" for="Message">The message*</label>
                            <textarea class="form-input" id="Message" name="message" placeholder="Lorem ipsum dolor sit amet..." rows="3">{{ old('message') }}</textarea>
                        </div>

                        @if ($errors->has('message'))
                        <div class="toast toast-danger">
                            <strong>{{ $errors->first('message') }}</strong>
                        </div>
                        @endif
                        <button type="button" class="btn btn-primary btn-blocking"><i class="omi omi-send-mail"></i> Send this message</button>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>

<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
    ga('create', 'UA-80736096-1', 'auto');
    ga('send', 'pageview');
</script>
</body>
</html>