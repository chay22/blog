@extends('layouts.blog')

@section('blog', true)
@section('title', 'Web Development Stories from a Learner')

@section('content')
<main id="blog" class="blog" itemprop="hasPart" itemscope itemtype="http://schema.org/Blog">
    <h1 class="title">The Web <span>Development</span> Log</h1>
    <div class="blog-quotes">I write something so I learn. It's not like people should actually read or know.</div>

    <section class="tags column col-10 center text-center">
        <h2>EXPLORE BY TAG</h2>
        <i class="omi omi-tag tagicon"></i>
        <ul id="tagList">    
            <li class="tag php"><a href="{{ route('tag', 'php') }}"><i class="omi omi-php"></i> PHP</a></li>
            <li class="tag laravel"><a href="{{ route('tag', 'laravel') }}"><i class="omi omi-laravel"></i> Laravel</a></li>
            <li class="tag javascript"><a href="{{ route('tag', 'javascript') }}"><i class="omi omi-javascript"></i> JavaScript</a></li>
            <li class="tag jquery"><a href="{{ route('tag', 'jquery') }}"><i class="omi omi-jquery"></i> jQuery</a></li>
            <li class="tag vue"><a href="{{ route('tag', 'vue') }}"><i class="omi omi-vue"></i> Vue.js</a></li>
            <li class="tag css"><a href="{{ route('tag', 'css') }}"><i class="omi omi-css"></i> CSS</a></li>
            <li class="tag html"><a href="{{ route('tag', 'html') }}"><i class="omi omi-html"></i> HTML</a></li>
        </ul>
    </section>

    <section class="blog-post column col-xs-11 col-sm-10 center">
        <h2>CHUNKS OF MY BLOG</h2>
        <i class="omi omi-pin-down posticon"></i>
        <div class="post-content">

        @foreach($posts as $post)
            <article id="blogpost{{ $post->id }}" class="post column" itemprop="blogPosts" itemscope itemtype="http://schema.org/BlogPosting">
                <div class="the-content columns col-lg-8">
                    <header>
                        <h3 class="post-title" itemprop="mainEntityOfPage headline" id="post{{ $post->id }}"><a rel="bookmark" href="{{ route('blog.post', $post->slug) }}">{{ $post->title }}</a></h3>
                        <dl>
                            <dt>{{ $post->updated_at == $post->created_at ? 'Posted on' : 'Updated at' }}</dt>
                            <dd><time itemprop="datePublished">{{ $post->updated_at === $post->created_at ? $post->created_at : $post->updated_at }}</time></dd>
                            <dt>by</dt>
                            <dd><span itemprop="author">{{ $post->user->name }}</span></dd>
                        </dl>
                    </header>

                    <div class="excerpt column">
                        <p itemprop="description">
                            {!! $post->excerpt !!}
                        </p>
                    </div>

                    <a href="{{ route('blog.post', $post->slug) }}" class="read-more" aria-describedby="post{{ $post->id }}" title="Read more">Continue reading...</a>
                    
                    <footer>
                        <ul class="tags-list" aria-label="{{ count($post->tags) > 1 ? 'Tags' : 'Tag' }}" itemprop="keywords">
                            @foreach($post->tags as $tag)
                            <li class="chip-sm"><a href="{{ route('tag', $tag->slug) }}" rel="category"><span class="hashtag">#</span>{{ $tag->name }}</a></li>
                            @endforeach
                        </ul>
                        
                        <span class="hide" aria-hidden="true" itemprop="dateModified">{{ $post->updated_at === $post->created_at ? $post->created_at : $post->updated_at }}</span>
                    </footer>
                </div>

                <div class="the-image column col-lg-4" itemprop="image" itemscope itemtype="http://schema.org/ImageObject">
                    <a class="image-link" href="{{ route('blog.post', $post->slug) }}" title="Read more: {{ $post->title }}">
                        <img class="img-responsive rounded" itemprop="image" alt="Image: {{ str_limit(strtolower($post->title), 40) }}" src="{{ url('/images').'/'.$post->image }}">
                    </a>
                    <meta itemprop="url" content="{{ url('/images').'/'.$post->image }}">
                    <meta itemprop="width" content="600">
                    <meta itemprop="height" content="300">
                </div>

                <div itemprop="publisher" itemscope itemtype="http://schema.org/Organization">
                    <div itemprop="logo" itemscope itemtype="http://schema.org/ImageObject">
                        <meta itemprop="url" content="{{ url('/android-chrome-192x192.png') }}">
                        <meta itemprop="width" content="192">
                        <meta itemprop="height" content="192">
                    </div>
                    <meta itemprop="name" content="Chay" itemscope itemprop="http://schema.org/Person">
                </div>
            </article>
        @endforeach
        </div>
    </section>
</main>
@endsection

@section('breadcrumb')
<div itemprop="breadcrumb" itemscope itemtype="http://schema.org/BreadCrumbList">
    <div itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
        <meta itemprop="position" content="1">
        <a href="{{ url('/') }}" itemprop="item">
            <meta itemprop="name" content="Home">
        </a>
    </div>
    <div itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
        <meta itemprop="position" content="2">
        <a href="{{ route('blog') }}" itemprop="item">
            <meta itemprop="name" content="Blog">
        </a>
    </div>
</div>
@endsection