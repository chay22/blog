@extends('layouts.app')

@section('blog', true)
@section('title', 'Create New Blog Post')

@section('content')
<main id="blog" class="blog">
    <markdown></markdown>
</main>

@endsection
