@extends('layouts.post')

@section('blog', true)
@section('title', $post->title)

@section('content')
<article class="single" itemprop="blogPost" itemscope itemtype="http://schema.org/BlogPosting">
    <link itemprop="mainEntityOfPage" href="#post" />
    <header>
        <i class="omi omi-{{ $post->tags[0]->slug }} iconback"></i> 
        <h1 class="title" itemprop="headline">{{ $post->title }}</h1>
    </header>

    <div class="block-separator rel"></div>

    <div class="the-post column col-xs-11 col-sm-10 col-md-9 col-lg-8">
        <div class="main-image col-sm-10 col-lg-9 center"itemprop="image" itemscope itemtype="http://schema.org/ImageObject">
            <img class="img-responsive" itemprop="image" alt="Image: {{ str_limit(strtolower($post->title), 40) }}" src="{{ url('/images').'/'.$post->image }}">
            <meta itemprop="url" content="{{ url('/images').'/'.$post->image }}">
            <meta itemprop="width" content="600">
            <meta itemprop="height" content="275">
        </div>

        <dl>
            <dt>{{ $post->updated_at === $post->created_at ? 'Posted on' : 'Updated at' }}</dt>
            <dd><time itemprop="datePublished">{{ $post->updated_at == $post->created_at ? $post->created_at : $post->updated_at }}</time></dd>
            <dt>by</dt>
            <dd><span itemprop="author">{{ $post->user->name }}</span></dd>
        </dl>

        <div class="the-post-content" itemprop="articleBody">
            {!! $post->content !!}
        </div>


        <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
            <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                <meta itemprop="url" content="{{ url('/') }}/android-chrome-192x192.png">
                <meta itemprop="width" content="192">
                <meta itemprop="height" content="192">
            </div>
            <meta itemprop="name" content="Chay">
        </div>
    </div>

    @if(isset($post->series))
    <aside class="in-series column col-xs-11 col-sm-10 col-md-9 col-lg-8 center">
        <h1>Series: <strong>{{ $post->series->title }}</strong></h1>
        <nav>
            <ol>
                @foreach($post->series->posts as $series)
                <li class="{{ $post->title == $series->title ? 'current-series' : '' }}">{{ $series->title }}</li>
                @endforeach
            </ol>
        </nav>
    </aside>
    @endif

    <footer class="the-footer col-xs-11 col-sm-10 col-md-9 col-lg-8 center">
        <ul class="tags-list" aria-label="{{ count($post->tags) > 1 ? 'Tags' : 'Tag' }}" itemprop="keywords">
            @foreach($post->tags as $tag)
            <li class="chip-sm"><a href="{{ route('tag', $tag->slug) }}" rel="category"><span class="hashtag">#</span>{{ $tag->name }}</a></li>
            @endforeach
        </ul>

        <div class="tagicon">
            <span class="omi omi-tags"></span>
        </div>

        <span class="hide" aria-hidden="true" itemprop="dateModified">2/19/2016</span>
    </footer>
</article>


<div class="after-main-post">
    @if (count($related) > 1)
    <aside class="related-posts col-xs-11 col-sm-10 col-md-9 col-lg-8 center">
        <h1>RELATED POSTS</h1>
        <div class="articles columns">
            <article class="article article-left column">
                <a rel="bookmark" href="{{ route('blog.post', $related[0]->slug) }}">
                    <h1>{{ $related[0]->title }}</h1>
                    <img class="img-responsive rounded" alt="{{ str_limit(strtolower($related[0]->title), 40) }}" src="{{ url('/images').'/'.$related[0]->image }}">
                </a>
            </article>

            <article class="article article-right column">
                <a rel="bookmark" href="{{ route('blog.post', $related[1]->slug) }}">
                    <h1>{{ $related[1]->title }}</h1>
                    <img class="img-responsive rounded" alt="{{ str_limit(strtolower($related[1]->title), 40) }}" src="{{ url('/images').'/'.$related[1]->image }}">
                </a>
            </article>
        </div>
        @if (count($related) > 2)
        <div class="articles columns">
            <article class="article article-left column">
                <a rel="bookmark" href="{{ route('blog.post', $related[2]->slug) }}">
                    <h1>{{ $related[2]->title }}</h1>
                    <img class="img-responsive rounded" alt="{{ str_limit(strtolower($related[2]->title), 40) }}" src="{{ url('/images').'/'.$related[2]->image }}">
                </a>
            </article>

            <article class="article article-right column">
                <a rel="bookmark" href="{{ route('blog.post', $related[3]->slug) }}">
                    <h1>{{ $related[3]->title }}</h1>
                    <img class="img-responsive rounded" alt="{{ str_limit(strtolower($related[3]->title), 40) }}" src="{{ url('/images').'/'.$related[3]->image }}">
                </a>
            </article>
        </div>
        @endif
    </aside>
    @endif
</div>
@endsection
