@extends('layouts.app')

@section('blog', true)
@section('title', 'Post tagged #'. $tag->name)

@section('content')
<main id="tag" class="tag-post">
    <i class="omi omi-{{ $tag->slug }} backicon {{ $tag->slug }}"></i>
    <header>
        <h1 class="title"><span class="hashtag">#</span>{{ $tag->name }}</h1>
    </header>

    <div class="block-separator"></div>

    <section class="blog-post column col-xs-11 col-sm-10 center" itemscope itemtype="http://schema.org/Blog">
        <h2>POST CONTAINING {{ strtoupper($tag->name) }}</h2>
        <div class="post-content">
        @if (count($posts) > 0)
        @foreach ($posts as $post)
            <article id="blogpost{{ $post->id }}" class="post column" itemprop="blogPosts" itemscope itemtype="http://schema.org/BlogPosting">
                <div class="the-content columns col-lg-8">
                    <header>
                        <h3 class="post-title" itemprop="mainEntityOfPage headline" id="post{{ $post->id }}"><a rel="bookmark" href="{{ route('blog.post', $post->slug) }}">{{ $post->title }}</a></h3>
                        <dl>
                            <dt>{{ $post->updated_at == $post->created_at ? 'Posted on' : 'Updated at' }}</dt>
                            <dd><time itemprop="datePublished">{{ $post->updated_at === $post->created_at ? $post->created_at : $post->updated_at }}</time></dd>
                            <dt>by</dt>
                            <dd><span itemprop="author">{{ $post->user->name }}</span></dd>
                        </dl>
                    </header>

                    <div class="excerpt column">
                        <p itemprop="description">
                            {!! $post->excerpt !!}
                        </p>
                    </div>

                    <a href="{{ route('blog.post', $post->slug) }}" class="read-more" aria-describedby="post{{ $post->id }}" title="Read more">Continue reading...</a>
                    
                    <footer>
                        <ul class="tags-list" aria-label="{{ count($post->tags) > 1 ? 'Tags' : 'Tag' }}" itemprop="keywords">
                            @foreach($post->tags as $tag)
                            <li class="chip-sm"><a href="{{ route('tag', $tag->slug) }}" rel="category"><span class="hashtag">#</span>{{ $tag->name }}</a></li>
                            @endforeach
                        </ul>
                        
                        <span class="hide" aria-hidden="true" itemprop="dateModified">{{ $post->updated_at === $post->created_at ? $post->created_at : $post->updated_at }}</span>
                    </footer>
                </div>

                <div class="the-image column col-lg-4" itemprop="image" itemscope itemtype="http://schema.org/ImageObject">
                    <img class="img-responsive rounded" itemprop="image" alt="Image: {{ str_limit(strtolower($post->title), 40) }}" src="{{ url('/images').'/'.$post->image }}">
                    <meta itemprop="url" content="{{ url('/images').'/'.$post->image }}">
                    <meta itemprop="width" content="600">
                    <meta itemprop="height" content="300">
                </div>

                <div itemprop="publisher" itemscope itemtype="http://schema.org/Organization">
                    <div itemprop="logo" itemscope itemtype="http://schema.org/ImageObject">
                        <meta itemprop="url" content="{{ url('/android-chrome-192x192.png') }}">
                        <meta itemprop="width" content="192">
                        <meta itemprop="height" content="192">
                    </div>
                    <meta itemprop="name" content="Chay" itemscope itemprop="http://schema.org/Person">
                </div>
            </article>
            @endforeach
            @else
                <div class="empty center">
                    <p class="empty-title">Yass! I'm a completely lazy person!</p>
                    <p class="empty-meta">If you kind, please take a look at my <a href="/blog" title="blog">blog</a> page for other available tags.</p>
                </div>
            @endif
        </div>
    </section>
</main>
@endsection

@section('breadcrumb')
<div itemprop="breadcrumb" itemscope itemtype="http://schema.org/BreadCrumbList">
    <div itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
        <meta itemprop="position" content="1">
        <a href="{{ url('/') }}" itemprop="item">
            <meta itemprop="name" content="Home">
        </a>
    </div>
    <div itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
        <meta itemprop="position" content="2">
        <a href="{{ route('blog') }}" itemprop="item">
            <meta itemprop="name" content="Blog">
        </a>
    </div>
    <div itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
        <meta itemprop="position" content="2">
        <a href="{{ route('tag', $tag->slug) }}" itemprop="item">
            <meta itemprop="name" content="#{{ $tag->name }}">
        </a>
    </div>
</div>
@endsection